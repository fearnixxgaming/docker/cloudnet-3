#!/bin/bash

## Function definition ##

if [[ "$CLOUDNET_BETA" -eq "true" ]];
	CN_BRANCH="beta"
else
	CN_BRANCH="release"
fi
URL="https://cloudnetservice.eu/cloudnet/version/${CN_BRANCH}/${CLOUDNET_TAG}/CloudNet.zip"

function setup_cloudnet_wrapper() {
	if [[ -e "dist.zip" ]]; then
    	rm dist.zip
    fi
        
	echo "Downloading ĆloudNet"
	echo "Downloading: $URL"
	curl -o dist.zip -sSL $URL
        
    echo "Extracting archive..."
    unzip dist.zip -d ./
}

## RUNTIME ##
cd /home/container

if [ "${whoami}" == "container" ]; then
	git config --global user.email "technik@fearnixx.de"
	git config --global user.name "FearNixx Technik"
fi

BASEPATH="/home/container/"
export SERVER_JARFILE="${BASEPATH}/launcher.jar"
if [[ ! -e "${SERVER_JARFILE}" ]]; then
	setup_cloudnet
fi

# Output Current Java Version
java -version

# Make internal Docker IP address available to processes.
export INTERNAL_IP=`ip route get 1 | awk '{print $NF;exit}'`
echo "IP today: $INTERNAL_IP"

# Replace Startup Variables
MODIFIED_STARTUP=`eval echo $(echo ${STARTUP} | sed -e 's/{{/${/g' -e 's/}}/}/g')`
echo ":/home/container$ ${MODIFIED_STARTUP}"

# Run the Server
eval ${MODIFIED_STARTUP}

# Example startup line: java -Xmx{{SERVER_XMX}}M -Xms{{SERVER_XMS}}M {{EXTRA_JVM}} -jar {{SERVER_JARFILE}} {{EXTRA_APP}}
